#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides main class App
"""

import argparse
import logging
import re

from typing import Union


class AppArgParser(argparse.ArgumentParser):
    """Command line parser which separates context options from application options"""

    def __init__(self, **kwargs):
        super().__init__(kwargs)

        self._log = logging.getLogger(__name__)

        # context group
        self._group_context = self.add_argument_group('context')
        self._init_group_context()

        # context group
        self._group_options = self.add_argument_group('options')

    def _init_group_context(self):
        self._group_context.add_argument('-c', '--config', help='path to application configuration file')
        self._group_context.add_argument('-ll', '--logging_level', help='logging level', default='INFO')
        self._group_context.add_argument('--disable_report', help='Disable publishing report', action='store_true')
        self._group_context.add_argument('-P', help='Override configuration option', nargs="*", dest="override_config")

    def add_option(self, *args, **kwargs):
        self._group_options.add_argument(*args, **kwargs)

    def _format_override_option(self, options, option_name: str) -> Union[dict, None]:
        if option_name not in options:
            return None

        override_config_raw = options[option_name]
        if override_config_raw is None or len(override_config_raw) == 0:
            return None

        override_config = dict()
        for option in override_config_raw:
            matcher = re.match(r'^([^=]*)=(.*)$', option)

            # case : invalid option
            if matcher is None:
                self._log.warning(f"Invalid override property '{option}'. Must be key=value")
                continue

            # case : valid option
            override_config[matcher.group(1)] = matcher.group(2)
        return override_config

    def _format_options(self, options):
        # transform override config options to dict
        if 'override_config' in options:
            options['override_config'] = self._format_override_option(options, 'override_config')
        return options

    def parse_args(self, args=None, namespace=None):
        options = dict()
        options.update(vars(super(AppArgParser, self).parse_args(args, namespace)))
        self._format_options(options)
        return options

