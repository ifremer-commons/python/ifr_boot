#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides main class App
"""

import os
from collections import OrderedDict
from typing import Optional, Union, Any

from ifr_lib import ifr_yaml, ifr_template


class AppConfig(object):
    """Application configuration"""

    def __init__(self, override_file_path: Optional[str] = None, override_config: Optional[dict] = None):
        """Initializes workspace subdirs and load configuration

        Args:
            override_file_path(str, optional): path to configuration file (YAML, JSON)
            override_config(str|dict, optional): content to override default configuration.
              Keys are added to tokens.
        """

        # retrieve override configuration
        self.__override_config = override_config

        # retrieve override config file path
        self.__override_file_path = override_file_path

        # init attributes
        self._raw_content = None
        self._effective_content = None
        self._tokens = None

        # load content
        self.reload()

    def reload(self):
        """Loads configuration.

        1 - Load default configuration (internal)
        2 - Override with content of file_path
        3 - Load extra tokens files
        4 - Update main paths
        """
        # Default config
        default_config = """\
                version: "1.0"

                workspace_path: !!null

                temp_path: !!null
                
                extra_token_files: !!list []

                extra_tokens: !!null

                report:
                  # report id (unique)
                  id: '{datetime}_{module}_{script}_{host}_{pid}'
                  # list of report targets
                  #  - filesystem : publish report to filesystem
                  #  - rabbitmq : push report to RabbitMQ queue
                  # ex :
                  #   targets: [filesystem, rabbitmq]
                  #   targets: [filesystem]
                  #   targets: [rabbitmq]
                  #   targets: []
                  targets: [filesystem]
                  # filesystem configuration
                  filesystem:
                    # if null : workspace["report_path"]
                    path: !!null
                  # rabbitmq configuration
                  rabbitmq:
                    host: 'localhost'
                    port: 5672
                    ssl: False
                    user: !!null
                    password: !!null
                    virtual_host: "/"
                    queue_name: 'execution-report'
                    routing_key: !!null

                logging_options:
                  message_length: 150
                  colors:
                    app: thin_green
                    multiline: thin_white
                    item: thin_blue

                logging_config:
                  root:
                    handlers: [console, file]
                  handlers:
                    console:
                      class: logging.StreamHandler
                      formatter: console_fmt
                      stream: "ext://sys.stdout"   
                    file:
                      '()': 'ifr_lib.ifr_logging.IfrFileHandler'
                      formatter: file_fmt
                      filename: ${logging.log_file}
                      when: midnight  
                      backupCount: 10
                      compress: True
                  formatters:
                    console_fmt:
                      '()': 'ifr_lib.ifr_logging.IfrColorFormatter'
                      fmt: '%(purple)s%(asctime)s%(reset)s | %(log_color)s%(levelname).3s%(reset)s | %(message_log_color)s%(message)-${logging.message_length}s%(reset)s | %(thin_white)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s'
                      datefmt: '%d/%m/%y %H:%M:%S'
                    file_fmt:
                      '()': 'ifr_lib.ifr_logging.IfrNoColorFormatter'
                      fmt: '%(asctime)s | %(levelname).3s | %(message)-${logging.message_length}s'
                      datefmt: '%d/%m/%y %H:%M:%S'
                """
        self._raw_content = ifr_yaml.YamlConfig(content=default_config)

        # Override with constructor's config
        if self.__override_config is not None:
            self._raw_content.merge(self.__override_config)

        # Override with user's config
        if self.__override_file_path is not None:
            self._raw_content.merge(ifr_yaml.YamlConfig(file=self.__override_file_path))

        # Parse extra token list
        self._tokens = ifr_yaml.YamlConfig()
        for file_path in self._raw_content["extra_token_files"]:
            # retrieve absolute path from relative path
            if not os.path.isabs(file_path):
                file_path = os.path.join(os.path.dirname(self.__override_file_path), file_path)
            self._tokens.merge(ifr_yaml.YamlConfig(file=file_path))

        # add extra tokens
        self._tokens.merge(self._raw_content['extra_tokens'])

        # add logging options
        logging_options = OrderedDict()
        logging_options['logging'] = self._raw_content['logging_options']
        self._tokens.merge(logging_options)

        # update paths relative to workspace
        self.__build_effective_config()

    def __build_effective_config(self):
        self._effective_content = \
            ifr_yaml.YamlConfig(content=self._raw_content.as_dict(), replacements=self._tokens.leafs())

    def effective_config(self):
        return self._effective_content.to_yaml()

    def __getitem__(self, path: str):
        """Retrieves an element of the configuration.

        Dotted notation allowed (ex: "my.key.2")

        Args:
            path (str): item key
        """
        return self._effective_content[path]

    def __contains__(self, item):
        return item in self._effective_content

    def tokens(self):
        return self._tokens.leafs()

    def add_tokens(self, tokens: Optional[dict] = None):
        """Adds new tokens.

        Args:
            tokens (dict): tokens to add
        """
        tokens.update(
            {'config_file': os.path.abspath(self.__override_file_path) if self.__override_file_path else None}
        )
        self._tokens.merge(tokens)
        self.__build_effective_config()

    def replace_tokens(self, content: Union[str, dict]) -> Any:
        """Replaces tokens in a content

        Args:
            content (str/dict): a str or dict to replace tokens
        """
        renderer = ifr_template.BaseTemplateRenderer(j2_env_params=ifr_template.TOKEN_STYLE_DOLLAR)
        return renderer.interpolate(content, self.tokens())

    def logging_config(self):
        return self['logging_config']

    def logging_options(self):
        return self['logging_options']

    def leafs(self, path: str):
        return self._effective_content.leafs(path)

    def __setitem__(self, key, value):
        self._raw_content[key] = value
        self.__build_effective_config()

    def contains(self, item):
        return self._effective_content.contains(item)
