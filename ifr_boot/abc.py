import tempfile
import uuid
from ifr_lib import ifr_datetimes
import hashlib

print(tempfile.gettempdir())
print(hashlib.sha256((uuid.uuid4().hex + ifr_datetimes.datetime_to_str(pattern="%Y%m%d%H%M%S%f")).encode('utf-8')).hexdigest()[:10])


