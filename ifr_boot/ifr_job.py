#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides main class Job
"""

import logging
import os

from ifr_lib import ifr_yaml, ifr_files
from ifr_lib.ifr_exception import FileNotFoundException, ConfigurationException

from ifr_boot.ifr_app import App, AppArgParser, AppStatus


class JobArgParser(AppArgParser):

    """Command line parser which separates context options from application options"""
    def _init_group_context(self):
        super()._init_group_context()
        self._group_context.add_argument('-jc', '--job_config', help='path to job configuration file')
        self._group_context.add_argument('-J', help='Override job option', nargs="*", dest="override_job")

    def _format_options(self, options):
        options = super()._format_options(options)
        if 'override_job' in options:
            options['override_job'] = self._format_override_option(options, 'override_job')
        return


class Job(App):

    #: job configuration extension
    EXT_JOB_CONFIG = ".yaml"

    #: job template extension
    EXT_JOB_TPL = ".tpl"

    @staticmethod
    def default_parser(description=None):
        return JobArgParser(description=None)

    def __init__(self,
                 name=None,
                 project_name=None,
                 config=None,
                 job_config=None,
                 logging_level='INFO',
                 override_config=None,
                 override_job=None,
                 **options):

        # retrieve project (use in _init_app_info)
        if not hasattr(self, 'args'):
            self.args = dict()

        self.args["project_name"] = project_name
        self.args["job_config_file"] = job_config

        # load constructor
        super(Job, self).__init__(
            name=name,
            config=config,
            logging_level=logging_level,
            override_config=override_config,
            **options)

        # manage override job config
        self.args["override_job"] = override_job

        # initialize attributes
        self.job_config = None
        self.job_template = None
        self.job_spool_path = None

    def _init_app_info(self):
        super(Job, self)._init_app_info()

        if self.args['project_name'] is not None:
            self.app_info['project_name'] = self.args['project_name']
        else:
            self.app_info['project_name'] = self.app_info['module_name']

    def _default_config(self):
        return {'job_config_path': None}

    def _init_context_info(self):
        super(Job, self)._init_context_info()
        self.context_info['job_config_path'] = self.config['job_config_path']
        if self.args['job_config_file'] is not None:
            self.context_info['job_config_file'] = os.path.abspath(self.args['job_config_file'])
        self.context_info['job_spool_path'] = self.job_spool_path

    def _init_main_config(self):
        if self.job_config is not None:
            self.main_config_info = self.job_config.leafs()

    def _add_config_tokens(self):
        super(Job, self)._add_config_tokens()
        self.config.add_tokens({
            'job_spool_path': self.job_spool_path
        })

    def _init_workspace(self):
        super(Job, self)._init_workspace()
        self.job_spool_path = ifr_files.to_unix_path(os.path.join(self.workspace['spool_path'], self.app_info['name']))

    def _setup(self):
        super(Job, self)._setup()

        # initialize job configuration
        self.init_job_config()

        # initialize job template
        self.init_job_template()

    def init_job_config(self):
        self.job_config = ifr_yaml.YamlConfig()

        # retrieve replacement tokens
        replacements = self.config.tokens()

        # load default configuration
        embedded_job_config = os.path.join(
            self.app_info['script_dir'],
            self.app_info['script_name'] + self.EXT_JOB_CONFIG
        )

        if os.path.isfile(embedded_job_config):
            self._log.debug(f"Load job configuration from file : {embedded_job_config}")
            self.job_config = ifr_yaml.YamlConfig(file=embedded_job_config, replacements=replacements)

        # load override job config for external file
        if self.args['job_config_file'] is not None:
            if not os.path.isfile(self.args['job_config_file']):
                raise FileNotFoundException("job config file dir does not exist : {}"
                                            .format(self.args['job_config_file']))
            self._log.debug("Override job configuration from file : {}".
                            format(os.path.abspath(self.args['job_config_file'])))
            self.job_config.merge(ifr_yaml.YamlConfig(file=self.args['job_config_file'], replacements=replacements))
        elif self.config['job_config_path'] is not None:

            if not os.path.isdir(self.config['job_config_path']):
                raise FileNotFoundException("job_config_path dir does not exist : {}".format(
                    self.config['job_config_path']))

            override_job_config = os.path.join(
                os.path.abspath(self.config['job_config_path']),
                self.app_info['project_name'],
                self.app_info['script_name'] + self.EXT_JOB_CONFIG
            )
            if os.path.isfile(override_job_config):
                self._log.debug(f"Override job configuration from file : {override_job_config}")
                self.job_config.merge(ifr_yaml.YamlConfig(file=override_job_config, replacements=replacements))

        # override job config
        if self.args["override_job"] is not None and isinstance(self.args["override_job"], dict):
            self._log.debug("Override job configuration with override_job")
            for key, value in self.args["override_job"].items():
                self.job_config[key] = value

        # check orphaned tokens
        if self.job_config.orphan_keys is not None and len(self.job_config.orphan_keys) > 0:
            self._set_status(AppStatus.ERROR)
            self._log.error("Orphaned keys : {} ".format(self.job_config.orphan_keys))
            raise ConfigurationException("Job configuration contains orphaned keys")

    def init_job_template(self):
        template_file_path = os.path.join(
            self.app_info['script_dir'],
            self.app_info['script_name'] + self.EXT_JOB_TPL
        )

        if os.path.isfile(template_file_path):
            self._log.debug(f"Template file found : {template_file_path}")
            self.job_template = template_file_path

    def _do(self):
        """Implements you work here."""
        print(self.config['logging_options.message_length'])


def main():
    """Job launching method"""
    parser = Job.default_parser()
    job = Job(**parser.parse_args())
    job.run()


if __name__ == '__main__':
    main()
