#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides app enumerations
"""

import signal
from enum import Enum

from ifr_lib.ifr_enum import ComparableEnum


class AppStatus(ComparableEnum):
    """Application status enumeration"""
    RUNNING = 0
    SUCCESS = 1
    WARNING = 2
    ERROR = 3


class AppSignal(Enum):
    """Application signal enumeration"""
    EXIT = [signal.SIGTERM.value if not isinstance(signal.SIGTERM, int) else signal.SIGTERM,
            signal.SIGINT.value if not isinstance(signal.SIGINT, int) else signal.SIGINT]

    # on windows SIGHUP, SIGUSR1/2 are not available
    if hasattr(signal, 'SIGHUP'):
        RELOAD = [signal.SIGHUP.value if not isinstance(signal.SIGHUP, int) else signal.SIGHUP]
    else:
        RELOAD = None


class AppLoggingColor(Enum):
    """Logging colors enumeration"""
    COLOR_APP = 'app'
    COLOR_MULTILINE = 'multiline'
    COLOR_ITEM = 'item'
