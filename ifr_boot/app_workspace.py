#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides main class Workspace
"""

import os

from ifr_lib import ifr_yaml, ifr_os, ifr_exception, ifr_files


class Workspace(object):

    def __init__(self, root_dir=None, structure=None):
        # retrieve workspace root dir
        self.root_dir = root_dir

        # retrieve workspace subdirs
        self.structure = ifr_yaml.YamlConfig(content={
            "log_path": "log",
            "spool_path": "spool",
            "report_path": "report"
        })

        if structure is not None:
            self.structure.merge(structure)

        # initialize absolute paths
        self.__absolute_path = dict()
        self._update_absolute_path()

        # create workspace tree
        self.__create_workspace_subdirs()

    def __create_workspace_subdirs(self):
        for sd_name, sd_path in self.__absolute_path.items():
            ifr_os.mkdirs(sd_path)

    @property
    def paths(self):
        return self.__absolute_path

    def __getitem__(self, item):
        if item not in self.__absolute_path:
            raise ifr_exception.ItemNotFoundException(f"Path {item} does not exists.")

        return self.__absolute_path[item]

    def _update_absolute_path(self):
        self.__absolute_path["root_path"] = ifr_files.to_unix_path(self.root_dir)

        for sd_name in self.structure.keys():
            if os.path.isabs(self.structure[sd_name]):
                self.__absolute_path[sd_name] = ifr_files.to_unix_path(self.structure[sd_name])
            else:
                self.__absolute_path[sd_name] = ifr_files.to_unix_path(
                    os.path.join(self.root_dir, self.structure[sd_name])
                )
