import os
import subprocess

from ifr_lib import ifr_datetimes
from ifr_lib.ifr_exception import GenericException

from ifr_boot.ifr_app import App, AppArgParser, AppStatus


class NumberOfInstanceException(GenericException):
    def __init__(self, *args, **kwargs):
        super(NumberOfInstanceException, self).__init__(*args, **kwargs)


class RunJobApp(App):

    @staticmethod
    def default_parser():
        parser = AppArgParser(description='Run command')
        parser.add_option('-i', '--id', help='job id', required=True)
        parser.add_option('-p', '--parallel', help='Number of parallel executions', default=0, type=int)
        parser.add_option('-e', '--env', help='Env vars', nargs='*')
        parser.add_option('-r', '--reference', help='Root task reference')
        parser.add_option('--shell', help='Use shell', action='store_true')
        parser.add_option('command', help='command to execute', nargs='+')
        return parser

    def _default_ws_structure(self):
        ws_structure = super(RunJobApp, self)._default_ws_structure()
        ws_structure["job_root_path"] = os.path.join("spool", self.options['id'].replace('.', os.sep))
        ws_structure["job_pid_path"] = os.path.join(ws_structure["job_root_path"], 'pid')
        return ws_structure

    def _initialize(self):
        super(RunJobApp, self)._initialize()

        # build pid filename
        self.pid_file = os.path.join(
            self.workspace['job_pid_path'],
            f"{ifr_datetimes.datetime_to_str(pattern='%Y%m%d%H%M%S')}"
            f"_{self.exec_info['hostname']}"
            f"_{self.exec_info['pid']}"
        )

        self.exec_info['reference'] = self.options['reference']

    def _do(self):
        # check number of instances
        nb_instances = self.__get_nb_instances()
        if nb_instances > self.options['parallel']:
            self._set_status(AppStatus.ERROR)
            raise NumberOfInstanceException(
                f"Number of instances : {nb_instances} ; max instances allowed : {self.options['parallel']}")

        # execute command
        self.__exec_command()

    def __get_nb_instances(self):
        return len(
            [name for name in os.listdir(self.workspace['job_pid_path'])
             if os.path.isfile(os.path.join(self.workspace['job_pid_path'], name))]
        )

    def __exec_command(self):

        # create pid file
        with open(self.pid_file, 'w') as f:
            f.write(
                f"jobid:{self.options['id']};"
                f" start={ifr_datetimes.datetime_to_iso()};"
                f" host:{self.exec_info['hostname']};"
                f" user={self.exec_info['user']};"
                f" pid={self.exec_info['pid']}"
            )

        # execute command
        try:
            # execute subprocess
            with subprocess.Popen(
                    self.options['command'],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    shell=self.options['shell']
            ) as p:
                output, errors = p.communicate()
                exit_status = p.returncode

            # manage stdout
            lines = output.decode('utf-8', 'backslashreplace').splitlines()
            if len(lines) > 0:
                for line in lines:
                    self._log.info(line.strip())

            # manage stderr
            lines = errors.decode('utf-8', 'backslashreplace').splitlines()
            if len(lines) > 0:
                for line in lines:
                    self._log.error(line.strip())

            # manage exit status
            if exit_status > 0:
                self._set_status(AppStatus.ERROR)
                self._log.error(f"Exit status > 0 : {exit_status}")
        except Exception as e:
            self._set_status(AppStatus.ERROR)
            self._log.error(f"Error during command execution : {str(e)}")

        # remove pid file
        os.remove(self.pid_file)


def main():
    """Application launching method"""
    parser = RunJobApp.default_parser()
    app = RunJobApp(**parser.parse_args())
    app.run()


if __name__ == '__main__':
    main()
