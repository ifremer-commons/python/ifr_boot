#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    ifr_boot.ifr_app
    ~~~~~~~~~~~~~~~~

    Provides main class App
"""


import logging
import os
import shlex
import shutil
import signal
import subprocess
import sys
import tempfile
import traceback
import uuid

from collections import OrderedDict
from typing import Optional, Union

from ifr_lib import ifr_misc, ifr_files, ifr_str, ifr_os, ifr_logging, ifr_datetimes
from ifr_lib.ifr_exception import GenericException, ItemNotFoundException
from ifr_lib.ifr_timer import Timers

from ifr_boot.app_argparser import AppArgParser
from ifr_boot.app_config import AppConfig
from ifr_boot.app_enum import AppStatus, AppLoggingColor, AppSignal
from ifr_boot.app_report import ExecutionReport, FileSystemReportVendor, RabbitMQReportVendor
from ifr_boot.app_workspace import Workspace


class App(object):
    """Standard application Template

    Attributes:
        context: application context (@see AppContext)
        config_to_export: main configuration items (context + application)
        options : application options
        results : application results
        timers: application timers (in order to retrieve duration)
    """

    @staticmethod
    def default_parser():
        """Defines default arguments parser"""
        return AppArgParser()

    def __init__(self,
                 name=None,
                 config=None,
                 logging_level: Optional[str] = 'INFO',
                 disable_report: Optional[bool] = False,
                 override_config: Optional[dict] = None,
                 **options):
        """Gets arguments and initializes workspace"""

        # init timers
        self.timers = Timers()
        self.timers.start("all")

        # retrieve args
        if not hasattr(self, 'args'):
            self.args = dict()

        self.args["name"] = name
        self.args["config_file"] = config
        self.args["logging_level"] = logging_level
        self.args["disable_report"] = disable_report
        self.args["override_config"] = override_config
        self.options = options if (options is not None and len(options) > 0) else dict()

        # init attributes
        self.app_info = OrderedDict()
        self.exec_info = OrderedDict()
        self.context_info = OrderedDict()
        self.main_config_info = OrderedDict()
        self._report_vendors = OrderedDict()

        self.config = None
        self.workspace = None
        self._log = None
        self.__status = None
        self.report_output_file_path = None
        self.results = OrderedDict()

        # register signal handlers
        self._sig_exit = False
        self._sig_reload = False

        if AppSignal.EXIT.value is not None:
            for sig in AppSignal.EXIT.value:
                signal.signal(sig, self._handle_signal)

        if AppSignal.RELOAD.value is not None:
            for sig in AppSignal.RELOAD.value:
                signal.signal(sig, self._handle_signal)

    def __instance_class_module(self):
        """Retrieves instance class package name"""
        module = self.__class__.__module__
        if module == '__main__':
            return ifr_files.parent_basename(sys.argv[0]) + \
                   "." +\
                   ifr_files.strip_extension(os.path.basename((sys.argv[0])))
        return module

    def _init_app_info(self):

        # retrieve class instance module and path
        instance_script = self.__instance_class_module()

        # retrieve module information
        self.app_info['module_name'] = instance_script.split(".")[0]
        self.app_info['module_version'] = ifr_misc.package_version(self.app_info['module_name'])

        # retrieve script name
        self.app_info['script_name'] = instance_script.rsplit(".", 1)[1]
        self.app_info['script_file'] = self.app_info['script_name'] + ".py"

        # retrieve script directory
        script_path = instance_script.replace(".", os.sep) + ".py"
        try:
            self.app_info['script_dir'] = ifr_files.parent_dir(ifr_files.find_file_in_syspath(script_path))
        except ItemNotFoundException:
            current_dir = os.getcwd()
            if not os.path.isfile(os.path.join(current_dir, self.app_info['script_file'])):
                raise
            self.app_info['script_dir'] = current_dir

        # retrieve virtual env infos
        if (os.sep + 'lib' + os.sep) in self.app_info['script_dir']:
            self.app_info['virtual_env_path'] = os.path.abspath(
                self.app_info['script_dir'].split(os.sep + 'lib' + os.sep)[0])
            self.app_info['virtual_env_name'] = os.path.basename(self.app_info['virtual_env_path'])
        else:
            self.app_info['virtual_env_path'] = None
            self.app_info['virtual_env_name'] = None

        # application name
        self.app_info['name'] = self.args['name'] if self.args['name'] is not None else self.__class__.__name__

    def _init_exec_info(self):
        self.exec_info = ifr_os.execution_info()
        self.exec_info['start_datetime'] = self.timers['all'].start_datetime

    def _init_config(self):
        self.config = AppConfig(
            override_file_path=self.args["config_file"],
            override_config=self._default_config()
        )

        if self.args["override_config"] is not None and isinstance(self.args["override_config"], dict):
            for key, value in self.args["override_config"].items():
                self.config[key] = value

        # init temp path
        if self.config['temp_path'] is None:
            self.config['temp_path'] = tempfile.gettempdir()

    def gettempdir(self, create_dir: bool = True):

        while True:
            temporary_dir = os.path.join(
                self.config['temp_path'],
                self.app_info['module_name'].lower() + "_" + str(uuid.uuid4().hex)
            )

            try:
                if create_dir:
                    os.makedirs(temporary_dir)
                    return temporary_dir
            except OSError:
                continue

            if not os.path.exists(temporary_dir):
                return temporary_dir

    def _default_config(self):
        return OrderedDict()

    def _init_workspace(self):
        # retrieve or define workspace root path
        ws_root_dir = self.config['workspace_path']
        if ws_root_dir is None:
            ws_root_dir = os.path.join(tempfile.gettempdir(), self.app_info['module_name'].lower())
            ifr_os.mkdirs(ws_root_dir)
        else:
            ws_root_dir = os.path.abspath(ws_root_dir)

        self.workspace = Workspace(
            root_dir=ws_root_dir,
            structure=self._default_ws_structure()
        )

        workspace_tokens = {'workspace': {}}
        for path_name in self.workspace.paths:
            workspace_tokens['workspace'][path_name] = self.workspace.paths[path_name]
        self.config.add_tokens(workspace_tokens)

    def _default_ws_structure(self):
        return OrderedDict()

    def _init_logging(self):
        # init logging dictionary
        self.logging_options = self.config.logging_options()
        self.logging_options['level'] = ifr_logging.level_name(self.args["logging_level"])

        # init log file
        self.__log_file = self._log_file()
        self.logging_options['log_file'] = self.__log_file
        self.config.add_tokens({'logging': {'log_file': self.__log_file}})

        # init logging
        ifr_logging.setup_logging(self.config.logging_config(), self.logging_options['level'])

        # define logger
        self._init_logger()

    def _log_file(self):
        logging.setLoggerClass(ifr_logging.ColorLogger)
        return ifr_files.to_unix_path(os.path.join(
            self.workspace.paths['log_path'],
            self.app_info['name'] + ".log"
        ))

    def _init_logger(self):
        self._log = logging.getLogger(__name__)
        self._log.message_length = self.logging_options['message_length']

    def _init_context_info(self):
        self.context_info['workspace_path'] = self.workspace.root_dir
        self.context_info['log_file'] = self.__log_file

    def _add_config_tokens(self):
        self.config.add_tokens({
            'virtual_env_path':  self.app_info['virtual_env_path'],
            'virtual_env_name': self.app_info['virtual_env_name'],
            'temp_path': self.config['temp_path']
        })

    def _set_status(self, status):
        """Upgrades application status if status is greater"""
        if self.__status is None or status > self.__status:
            self.__status = status

    def exit_status(self):
        """Exits application with the good exit_status status"""
        return 0 if self.__status in [AppStatus.SUCCESS, AppStatus.WARNING] else 1

    def _handle_signal(self, signum, frame):
        """Does stuff if signal is emitted"""
        self._log.debug(f"Receive signal {signum}")
        if signum in AppSignal.EXIT.value:
            self._handle_exit()
        elif signum in AppSignal.RELOAD.value:
            self._handle_reload()

    def _handle_exit(self):
        """Does stuff if exit_status signal is emitted"""
        self._sig_exit = True

    def _handle_reload(self):
        """Does stuff if reload signal is emitted"""
        self._sig_reload = True

    def _init_main_config(self):
        pass

    def _get_color(self, name: AppLoggingColor) -> Union[str, None]:
        if isinstance(name, AppLoggingColor):
            name = name.value

        if name in self.logging_options['colors']:
            return self.logging_options['colors'][name]
        return None

    def _colorize(self, value: str, color_name: AppLoggingColor):
        return ifr_str.colorize(value, self._get_color(color_name))

    def _print_msg_start(self):
        """Logs start message"""
        self._log.info('=', color=self._get_color(AppLoggingColor.COLOR_APP), repeat=True)
        self._log.info(f"Start of {self.app_info['name']} (version : {self.app_info['module_version']})",
                       color=self._get_color(AppLoggingColor.COLOR_APP))
        self._log.info('=', color=self._get_color(AppLoggingColor.COLOR_APP), repeat=True)

    def _print_msg_end(self):
        """Logs end message"""
        # print results
        self._log.log_dict(logging.INFO, self.results, title='Results :', header='-', footer='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # print timers
        self._log.log_dict(logging.DEBUG, self.timers.durations(), title='Timers :', header='-', footer='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # define message and color
        log_fct = self._log.info
        color_opts = self._get_color(AppLoggingColor.COLOR_APP)
        msg_complement = ''
        if self.__status == AppStatus.WARNING:
            log_fct = self._log.warning
            color_opts = 'thin_yellow'
            msg_complement = 'with warnings '
        elif self.__status == AppStatus.ERROR:
            log_fct = self._log.error
            color_opts = 'thin_red'
            msg_complement = 'with errors '

        # print end message
        log_fct('=', repeat=True, color=color_opts)
        log_fct(f"End of {self.app_info['name']} {msg_complement}(elapsed time : {self.timers.duration('all')})",
                color=color_opts)
        log_fct('=', repeat=True, color=color_opts)

    def _initialize(self):
        pass

    def _setup(self):
        # application info
        self._init_app_info()

        # execution info
        self._init_exec_info()

        # configuration
        self._init_config()

        # init workspace
        self._init_workspace()

        # init logger
        self._init_logging()

        self._add_config_tokens()

        # print msg start
        self._print_msg_start()

        # print app info
        self._log.log_dict(logging.DEBUG, self.app_info, title='Application :', header='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # print execution information
        self._log.log_dict(logging.DEBUG, self.exec_info, title='Execution :',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # print main context info
        self._init_context_info()
        self._log.log_dict(logging.DEBUG, self.context_info, title='Context :', footer='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # init reporting providers
        if self.args["disable_report"] is False:
            for target in self.config['report.targets']:
                self._report_vendors[target] = self._report_vendor(target)

    def _report_vendor(self, provider_name: str):
        # filesystem report writer
        if 'filesystem' in provider_name:
            report_dir_path = self.workspace['report_path']
            if 'report.filesystem.path' in self.config and self.config['report.filesystem.path'] is not None:
                report_dir_path = self.config['report.filesystem.path']
            return FileSystemReportVendor(report_dir_path)

        # rabbitMQ report writer
        if 'rabbitmq' in self.config['report.targets']:
            return RabbitMQReportVendor(
                host=self.config['report.rabbitmq.host'],
                port=self.config['report.rabbitmq.port'],
                ssl=self.config['report.rabbitmq.ssl'],
                user=self.config['report.rabbitmq.user'],
                password=self.config['report.rabbitmq.password'],
                virtual_host=self.config['report.rabbitmq.virtual_host'],
                queue_name=self.config['report.rabbitmq.queue_name'],
                routing_key=self.config['report.rabbitmq.routing_key']
            )

    def _print_msg_setup(self):
        # print main configuration
        self._init_main_config()
        self._log.log_dict(logging.DEBUG, self.main_config_info, title='Configuration :', header='-', footer='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

        # print options
        self._log.log_dict(logging.DEBUG, self.options, title='Options :', header='-', footer='-',
                           color=self._get_color(AppLoggingColor.COLOR_MULTILINE))

    def _log_error(self, message: str, exc_info: Optional[bool] = False):
        self._set_status(AppStatus.ERROR)
        if self._log is None:
            traceback.print_exc()
        else:
            self._log.error(message, exc_info=exc_info)

    def _do(self):
        """Implements you work here."""
        pass

    def _finalize(self):
        pass

    def run(self, exit: Optional[bool] = True):
        """Runs the job

        Steps:
        - _setup() : initialize application data (configuration, options, ...)
        - _initialize() : pre treatment
        - _do(): do stuff
        - _finalize() : post treatment (close connections, ...)
        """
        self._set_status(AppStatus.RUNNING)

        try:
            # initialize application
            self.timers.start('setup')
            self._setup()
            self._print_msg_setup()
            self.timers.stop('setup')

            self.timers.start('initialize')
            self._initialize()
            self.timers.stop('initialize')

            # process job
            self.timers.start('do')
            self._do()
            self.timers.stop('do')
        except KeyboardInterrupt:
            self._set_status(AppStatus.ERROR)
            self._log.error('Process stopped with by the user (keyboard interruption)')
        except GenericException as err:
            self._log_error(str(err), exc_info=True)
        except Exception:
            self._log_error('An unknown exception has occurred', exc_info=True)
        finally:
            try:
                # post job process
                self.timers.start('finalize')
                self._finalize()
                self.timers.stop('finalize')

                # set status to success
                self._set_status(AppStatus.SUCCESS)
            except Exception:
                self._log_error('An exception has occurred during finalization', exc_info=True)
            finally:
                # end global timer
                self.timers.stop('all')

                # update execution info
                self.exec_info['end_datetime'] = self.timers['all'].end_datetime
                self.exec_info['duration'] = self.timers['all'].duration
                self.exec_info['exit_status'] = self.exit_status()

                # publish report
                if self.args["disable_report"] is False:
                    try:
                        self._publish_report()
                    except Exception:
                        self._log_error('An exception has occurred while publishing report', exc_info=True)

                # print final message
                if self._log is not None:
                    self._print_msg_end()

                if exit:
                    self.exit()

    def exit(self):
        exit(self.exit_status())

    def _build_report(self):
        report_id = ifr_str.str_format(
            self.config['report.id'],
            datetime=ifr_datetimes.datetime_to_str(self.timers['all'].start_datetime, '%Y%m%d%H%M%S'),
            module=self.app_info['module_name'],
            script=self.app_info['name'],
            host=self.exec_info['hostname'],
            pid=self.exec_info['pid'])

        return ExecutionReport(
            id=report_id,
            execution=self.exec_info,
            application=self.app_info,
            options=self.options,
            context=self.context_info,
            main_config=self.main_config_info,
            timers=self.timers.durations(),
            results=self.results
        )

    def report(self):
        return str(self._build_report())

    def _publish_report(self):
        # build report
        report = self._build_report()

        # publish report to provider
        for name, report_writer in self._report_vendors.items():
            try:
                self._log.debug(f"Publish report on '{name}'")
                report_writer.publish(report)
            except Exception:
                self._log.error(f"An exception has occurred while publishing report {name}", exc_info=True)

    def execute_command(self, cmd: str) -> int:
        """Run a command in as a subprocess and follow execution

        Params:
            cmd (str): command line

        Returns:
            int: exit code
        """
        # execute command line
        try:
            # execute subprocess
            with subprocess.Popen(
                shlex.split(cmd),
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
            ) as process:

                # manage stdout
                while True:
                    output = process.stdout.readline().decode('utf-8', 'backslashreplace')

                    # break loop if exit status is set
                    if output == '' and process.poll() is not None:
                        break

                    # print message
                    if output:
                        self._log.info(output.strip())

                # manage exits status
                exit_status = process.poll()

                if exit_status is None or exit_status > 0:
                    self._log_error(f"Exit status > 0 : {exit_status}")

                return exit_status
        except Exception:
            self._log_error('Error during command execution', exc_info=True)
            return 1


def main():
    """App launching method"""
    parser = App.default_parser()
    app = App(**parser.parse_args())
    app.run()


if __name__ == '__main__':
    main()
