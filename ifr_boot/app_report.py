#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

"""
    Provides an execution report
"""

import logging
import os
from typing import Optional

from ifr_lib import ifr_json, ifr_exception

# import RabbitMQ dependencies
try:
    import pika
except ImportError:
    pika = None


class ExecutionReport(ifr_json.JsonSerializable):
    """Execution report"""

    def __init__(self,
                 id,
                 execution: dict,
                 application: dict,
                 options: dict,
                 context: dict,
                 main_config: dict,
                 timers: dict,
                 results: dict):
        """Initializes the execution report

        Args:
            execution (dict): the application execution info
            application (dict): the application execution info
            options (dict): the application options info
            context (dict): the application context info
            main_config (dict): the application main_config info
            timers (dict): the application timers info
            results (dict): the application results info
        """
        self.id = id
        self.execution = execution
        self.application = application
        self.options = options
        self.context = context
        self.main_config = main_config
        self.timers = timers
        self.results = results

    def render(self):
        """The execution report's string representation

        Returns:
            The JSON representation of the execution report
        """
        return self.to_json(indent=2)

    @property
    def filename(self):
        return f"{self.id}.json"

    def __repr__(self):
        return self.render()

    def __str__(self):
        return self.__repr__()


class WriteReportException(ifr_exception.GenericException):
    def __init__(self, *args, **kwargs):
        super(WriteReportException, self).__init__(*args, **kwargs)


class ReportVendor(object):
    """Super class of all execution report writers."""

    def publish(self, report: ExecutionReport):
        """Publish report to vendor

        Args:
            report (ExecutionReport): execution report model

        Raises:
            WriteReportException
        """
        pass


class FileSystemReportVendor(ReportVendor):
    """Execution report writer for file system."""

    def __init__(self, file_path):
        """Initializes the file system report writer.

        Args:
            file_path (str): The report output file path
        """
        self.file_path = file_path

    def publish(self, report: ExecutionReport):
        """Writes the execution report to the defined file_path.

        Args:
            report (ExecutionReport): execution report model

        Raises:
            WriteReportException
        """
        assert isinstance(report, ExecutionReport)

        try:
            report_file_path = os.path.join(self.file_path, report.filename)
            with open(report_file_path, 'w') as file:
                file.write(report.render())
        except Exception:
            raise WriteReportException(f"A problem occurred while writing report to filesystem {self.file_path}")


class RabbitMQReportVendor(ReportVendor):
    """Execution report writer for RabbitMQ"""

    def __init__(self,
                 host: Optional[str] = "localhost",
                 port: Optional[int] = 5672,
                 ssl: Optional[bool] = False,
                 user: Optional[str] = None,
                 password: Optional[str] = None,
                 virtual_host: Optional[str] = '/',
                 queue_name: Optional[str] = "execution-report",
                 routing_key: Optional[str] = None):
        """Initializes the RabbitMQ report writer.

        Args:
            host (str): The RabbitMQ host
            port (int): The RabbitMQ port
            queue_name (str): The RabbitMQ queue name
        """

        if pika is None:
            raise ImportError("Module pika is not found")

        # avoid rabbitMQ traces
        pika_logger = logging.getLogger("pika")
        pika_logger.setLevel(logging.WARNING)

        # store args
        self.__host = host
        self.__port = port
        self.__queue_name = queue_name
        self.__routing_key = routing_key if routing_key is not None else queue_name

        if not ssl:
            ssl_options = None
        # TODO : manage case when ssl is True
        else:
            ssl_options = pika.SSLOptions()

        credentials = pika.credentials.PlainCredentials(user, password) \
            if password is not None else pika.connection.Parameters.DEFAULT_CREDENTIALS

        # create connection to RabbitMQ server
        self.__connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=host,
                credentials=credentials,
                ssl_options=ssl_options,
                port=port,
                virtual_host=virtual_host,
            )
        )

        # create channel for queue
        self.__channel = self.__connection.channel()
        self.__channel.queue_declare(
            queue=self.__queue_name,
            durable=True,
        )

    def __del__(self):
        if hasattr(self, '__connection') and self.__connection is not None:
            self.__connection.close()

    def publish(self, report: ExecutionReport):
        """Broadcasts the execution report according to the defined RabbitMQ parameters.

        Args:
            report (ExecutionReport): execution report model

        Raises:
            WriteReportException
        """
        assert isinstance(report, ExecutionReport)

        try:
            self.__channel.basic_publish(
                exchange='',
                routing_key=self.__routing_key,
                properties=pika.spec.BasicProperties(
                    message_id=report.id,
                    content_type="ExecutionReport",
                ),
                body=report.render()
            )
        except Exception:
            raise WriteReportException(
                f"A problem occurred while publishing report to rabbitMQ ("
                f"host : {self.__host} ; port : {self.__port} : queue : {self.__queue_name})"
            )
