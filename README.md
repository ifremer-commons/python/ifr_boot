# ifr_boot

This module provides a base for developing applications and jobs.

To run a simple application or a job

`<myApp> [-c <configuration_file> -ll <log_level>]`

With :

- `configuration_file` : path to a configuration file that may contain YAML or JSON content
   - sample_app_config.yaml
   - sample_job_config.yaml
- `log_level` : root log level (in : DEBUG, INFO, WARNING, ERROR, FATAL)
    
## Development

### Install poetry

```bash
pip install poetry poetry-dynamic-versioning poetry2conda
poetry --version
poetry config repositories.nexus-public-release https://nexus-test.ifremer.fr/repository/hosted-pypi-public-release/
```

### retrieve and install project

```bash
git clone https://gitlab.ifremer.fr/ifremer-commons/python/ifr_boot.git
poetry install -v --no-root
```

### List dependencies

```bash
poetry show --tree
```

# build and publish wheel

```bash
poetry build --format wheel
poetry publish -r nexus-public-release -u nexus-ci -p w2bH2NjgFmQnzVk3
```
 
# build documentation

```
mkdocs build -f docs/mkdocs.yml
```
