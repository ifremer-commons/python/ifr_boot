# ifr_boot

This module provides a base for developing applications and jobs.

To run a simple application or a job

`<myApp> [-c <configuration_file> -ll <log_level>]`

With :

- `configuration_file` : path to a configuration file that may contain YAML or JSON content
   - sample_app_config.yaml
   - sample_job_config.yaml
- `log_level` : root log level (in : DEBUG, INFO, WARNING, ERROR, FATAL)